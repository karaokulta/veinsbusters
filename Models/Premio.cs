﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VeinsBuster.Models
{
    public class Premio
    {
        [Required]
        public int Id { get; set; }

        [StringLength(64)]
        public string Nombre { get; set; }

        [StringLength(128)]
        public string Descripcion { get; set; }

        public int Costo { get; set; }

        //[StringLength(255)]
        //public string Imagen { get; set; }
        
        public TipoPremio Tipo { get; set; }

        [Required]
        public byte TipoId { get; set; }

        public bool Estatus { get; set; }

    }
}