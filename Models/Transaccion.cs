﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VeinsBuster.Models
{
    public class Transaccion
    {
        public int Id { get; set; }

        public Usuario Usuario { get; set; }

        [Required]
        public int UsuarioId { get; set; }

        public Premio Premio { get; set; }

        [Required]
        public int PremioId { get; set; }

        public int Puntos { get; set; }

        public DateTime Fecha { get; set; }

        public DateTime? FechaUso { get; set; }

        public bool Estatus { get; set; }
    }
}