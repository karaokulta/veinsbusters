﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VeinsBuster.Models
{
    public class Usuario
    {
        [Required]
        public int Id { get; set; }

        [StringLength(64)]
        public string Nombre { get; set; }

        [StringLength(128)]
        public string Email { get; set; }

        [StringLength(64)]
        public string Password { get; set; }

        public double Puntos { get; set; }

        public int Invitaciones { get; set; }

        //public int InvitacionActivas { get; set; }

        public int InvitacionesPendientes { get; set; }

        public string Codigo { get; set; }

        //[StringLength(255)]
        //public string Imagen { get; set; }

        public bool Estatus { get; set; }
    }
}