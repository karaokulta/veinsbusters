﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VeinsBuster.Models
{
    public class Invitacion
    {
        
        public int Id { get; set; }

        public Usuario Usuario { get; set; }

        public int IdUsuario { get; set; }

        public Usuario Invitado { get; set; }

        public int IdInvitado { get; set; }

        public DateTime Fecha { get; set; }

        public bool Estatus { get; set; }

    }
}