﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VeinsBuster.Models
{
    public class TipoPremio
    {
        public byte Id { get; set; }

        [Required]
        [StringLength(32)]
        public string Nombre { get; set; }
    }
}