﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VeinsBuster.Models;
using VeinsBuster.Dtos;

namespace VeinsBuster.Controllers.Api
{
    public class UsuariosController : ApiController
    {
        private ApplicationDbContext _context;

        public UsuariosController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        // GET /api/usuario/1
        public Usuario GetUser(int id)
        {
            var user = _context.Usuarios.SingleOrDefault(u => u.Id == id);
            if (user == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            user.Password = "";
            return user;
        }

        // GET /api/user?Email=Mail&Password=Password
        public Usuario GetUser(string Email, string Password)
        {
            var user = _context.Usuarios.SingleOrDefault(u => u.Email == Email && u.Password == Password);
            if (user == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            user.Password = "";
            return user;
        }

        // POST /api/usuario
        [HttpPost]
        public Usuario CreateUser(NuevoUsuario newUser)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            var userInDb = _context.Usuarios.SingleOrDefault(u => u.Email == newUser.Email);
            if (userInDb != null)
                throw new HttpResponseException(HttpStatusCode.NotAcceptable);
            Usuario userInv = null;
            if (!string.IsNullOrEmpty(newUser.Codigo))
            {
                //userInv = _context.Usuarios.SingleOrDefault(u => u.Codigo == newUser.Codigo);
                userInv = _context.Usuarios.SingleOrDefault(u => u.Password == newUser.Codigo);
                if (userInv == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            var user = new Usuario
            {
                Nombre = newUser.Nombre,
                Email = newUser.Email,
                Password = newUser.Password,
                Puntos = 0,
                Invitaciones = 0,
                InvitacionesPendientes = 0,
                Codigo = "1234",
                Estatus = true
            };
            //Agregamos usuario
            _context.Usuarios.Add(user);
            _context.SaveChanges();
            //Agregamos invitacion
            if (userInv != null)
            {
                userInv.InvitacionesPendientes += 1;
                var invitacion = new Invitacion
                {
                    IdUsuario = userInv.Id,
                    IdInvitado = user.Id,
                    Fecha = DateTime.Now,
                    Estatus = false
                };
                _context.Invitaciones.Add(invitacion);
                _context.SaveChanges();
            }
            user.Password = "";
            return user;
        }

        // PUT /api/usuario/1
        [HttpPut]
        public Usuario UpdateUser(int id, NuevoUsuario user)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            var userInDb = _context.Usuarios.SingleOrDefault(u => u.Email == user.Email);
            if (userInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            userInDb.Nombre = user.Nombre;
            userInDb.Email = user.Email;
            if (user.Password != "")
                userInDb.Password = user.Password;
            _context.SaveChanges();
            userInDb.Password = "";
            return userInDb;
        }

        private string GetCode()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var code = string.Empty;
            do
            {
                code = new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[random.Next(s.Length)]).ToArray());

            } while (null != _context.Usuarios.SingleOrDefault(u => u.Codigo == code));
            return code;
        }
    }
}
