﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VeinsBuster.Models;
using VeinsBuster.Dtos;
using System.Data.Entity;

namespace VeinsBuster.Controllers.Api
{
    public class TransaccionesController : ApiController
    {
        private ApplicationDbContext _context;

        public TransaccionesController()
        {
            _context = new ApplicationDbContext();
        }

        //GET /api/Transacciones/1
        public IEnumerable<Transaccion> GetTransacciones(int id)
        {
            return _context.Transacciones.Include(p => p.Premio).Where(t => t.UsuarioId == id).ToList();
        }


        //POST /api/Transacciones/
        [HttpPost]
        public IHttpActionResult SetTransaccion(AddTransaccion transaccion)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            var user = _context.Usuarios.SingleOrDefault(u => u.Id == transaccion.IdUsuario);
            if (user == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            var premio = _context.Premios.SingleOrDefault(p => p.Id == transaccion.IdPremio);
            if (premio == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            if (user.Puntos < premio.Costo)
                throw new HttpResponseException(HttpStatusCode.NotAcceptable);
            user.Puntos -= premio.Costo;
            var trans = new Transaccion
            {
                UsuarioId = user.Id,
                PremioId = premio.Id,
                Puntos = premio.Costo,
                Fecha = DateTime.Now,
                Estatus = false
            };
            _context.Transacciones.Add(trans);
            _context.SaveChanges();
            return Ok();
        }

        //PUT /api/Transacciones/1
        [HttpPut]
        public IHttpActionResult UpdateTransaccion(int id)
        {
            var trans = _context.Transacciones.SingleOrDefault(t => t.Id == id);
            if (trans == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            trans.FechaUso = DateTime.Now;
            trans.Estatus = true;
            _context.SaveChanges();
            return Ok();
        }

    }
}
