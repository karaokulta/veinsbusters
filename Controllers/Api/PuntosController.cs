﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VeinsBuster.Models;
using VeinsBuster.Dtos;

namespace VeinsBuster.Controllers.Api
{
    public class PuntosController : ApiController
    {
        private ApplicationDbContext _context;

        public PuntosController()
        {
            _context = new ApplicationDbContext();
        }
        
        //POST /api/Puntos
        [HttpPost]
        public IHttpActionResult SetPuntos(AddPuntos puntos)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            Usuario userInv = null;
            var user = _context.Usuarios.SingleOrDefault(u => u.Id == puntos.IdUsuario);
            if (user == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            user.Puntos += puntos.Puntos;
            var invitacion = _context.Invitaciones.SingleOrDefault(i => i.IdInvitado == user.Id && i.Estatus == false);
            if (invitacion != null)
            {
                userInv = _context.Usuarios.SingleOrDefault(u => u.Id == invitacion.IdUsuario);
                userInv.Invitaciones += 1;
                userInv.InvitacionesPendientes -= 1;
                invitacion.Estatus = true;
                var premio = _context.Premios.SingleOrDefault(p => p.Costo == userInv.Invitaciones && p.TipoId == 2);
                if (premio != null)
                {
                    var trans = new Transaccion
                    {
                        UsuarioId = userInv.Id,
                        PremioId = premio.Id,
                        Puntos = premio.Costo,
                        Fecha = DateTime.Now,
                        Estatus = false
                    };
                    _context.Transacciones.Add(trans);
                }
            }
            _context.SaveChanges();
            return Ok();
        }
    }
}
