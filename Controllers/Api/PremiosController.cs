﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VeinsBuster.Models;
using System.Data.Entity;

namespace VeinsBuster.Controllers.Api
{
    public class PremiosController : ApiController
    {
        private ApplicationDbContext _context;

        public PremiosController()
        {
            _context = new ApplicationDbContext();
        }

        //GET /api/Premios
        public IEnumerable<Premio> GetPremios()
        {
            return _context.Premios.Include(t => t.Tipo).Where(p => p.Estatus == true).ToList();
        }

        //GET /api/Premios?Nombre=Consulta
        public Premio GetPremio(string Nombre)
        {
            return _context.Premios.Include(t => t.Tipo).SingleOrDefault(p => p.Nombre == Nombre && p.Estatus == true);
        }
    }
}
