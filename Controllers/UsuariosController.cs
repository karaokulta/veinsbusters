﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VeinsBuster.Models;
using VeinsBuster.ViewModels;
using System.Data.Entity;

namespace VeinsBuster.Controllers
{
    public class UsuariosController : Controller
    {
        private ApplicationDbContext _context;

        public UsuariosController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

        // GET: Usuarios
        public ActionResult Index()
        {
            var users = _context.Usuarios.ToList();
            return View(users);
        }

        // GET: Usuarios/Buscar?nombre=Nestor
        public ActionResult Buscar(string nombre)
        {
            var users = _context.Usuarios.Where(u => u.Nombre.Contains(nombre)).ToList();
            return View("Index", users);
        }

        // GET: Usuarios/Perfil/1
        public ActionResult Perfil(int id)
        {
            var user = _context.Usuarios.SingleOrDefault(p => p.Id == id);
            if (user == null)
                return HttpNotFound();
            var viewModel = new PerfilViewModel
            {
                Usuario = user,
                TrPendientes = _context.Transacciones.Include(p => p.Premio).Where(t => t.UsuarioId == id && t.Estatus == false).ToList(),
                TrUsadas = _context.Transacciones.Include(p => p.Premio).Where(t => t.UsuarioId == id && t.Estatus == true).ToList()
            };
            return View(viewModel);
        }
    }
}