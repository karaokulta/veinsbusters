﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VeinsBuster.Models;
using System.Data.Entity;
using VeinsBuster.ViewModels;

namespace VeinsBuster.Controllers
{
    public class PremiosController : Controller
    {
        private ApplicationDbContext _context;

        public PremiosController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Premios
        public ActionResult Index()
        {
            return View(_context.Premios.Include(p => p.Tipo).ToList());
        }

        // GET: Prizes/Nuevo
        public ActionResult Nuevo()
        {
            var viewModel = new PremioFormViewModel
            {
                TiposPremio = _context.TipoPremio.ToList()
            };
            return View("PremioForm", viewModel);
        }

        // GET: Prizes/Editar/1
        public ActionResult Editar(int id)
        {
            var prize = _context.Premios.SingleOrDefault(p => p.Id == id);
            if (prize == null)
                return HttpNotFound();
            var viewModel = new PremioFormViewModel
            {
                Premio = prize,
                TiposPremio = _context.TipoPremio.ToList()
            };
            return View("PremioForm", viewModel);
        }

        // POST Prizes/Save
        [HttpPost]
        public ActionResult Save(Premio premio)
        {
            if (premio.Id == 0)
            {
                _context.Premios.Add(premio);
            }
            else
            {
                var prizeInDb = _context.Premios.Single(c => c.Id == premio.Id);
                prizeInDb.Nombre = premio.Nombre;
                prizeInDb.Descripcion = premio.Descripcion;
                prizeInDb.Costo = premio.Costo;
                prizeInDb.Estatus = premio.Estatus;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Premios");
        }
    }
}