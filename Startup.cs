﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VeinsBuster.Startup))]
namespace VeinsBuster
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
