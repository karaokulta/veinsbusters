﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VeinsBuster.Models;

namespace VeinsBuster.ViewModels
{
    public class PremioFormViewModel
    {
        public IEnumerable<TipoPremio> TiposPremio { get; set; }

        public Premio Premio { get; set; }

        public string Title
        {
            get
            {
                if (Premio != null && Premio.Id != 0)
                    return "Editar Premio";
                return "Premio Nuevo";
            }
        }
    }
}