﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VeinsBuster.Models;

namespace VeinsBuster.ViewModels
{
    public class PerfilViewModel
    {
        public Usuario Usuario { get; set; }

        public IEnumerable<Transaccion> TrPendientes { get; set; }

        public IEnumerable<Transaccion> TrUsadas { get; set; }

    }
}