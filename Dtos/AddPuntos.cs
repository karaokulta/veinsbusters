﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VeinsBuster.Dtos
{
    public class AddPuntos
    {
        public int IdUsuario { get; set; }
        public double Puntos { get; set; }
    }
}