namespace VeinsBuster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transaccions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        PremioId = c.Int(nullable: false),
                        Puntos = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        FechaUso = c.DateTime(nullable: false),
                        Estatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Premios", t => t.PremioId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId)
                .Index(t => t.PremioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transaccions", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Transaccions", "PremioId", "dbo.Premios");
            DropIndex("dbo.Transaccions", new[] { "PremioId" });
            DropIndex("dbo.Transaccions", new[] { "UsuarioId" });
            DropTable("dbo.Transaccions");
        }
    }
}
