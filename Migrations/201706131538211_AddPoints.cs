namespace VeinsBuster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPoints : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invitaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdUsuario = c.Int(nullable: false),
                        IdInvitado = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        Invitado_Id = c.Int(),
                        Usuario_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.Invitado_Id)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_Id)
                .Index(t => t.Invitado_Id)
                .Index(t => t.Usuario_Id);
            
            AddColumn("dbo.Usuarios", "Invitaciones", c => c.Int(nullable: false));
            DropColumn("dbo.Usuarios", "InvitacionActivas");
            DropColumn("dbo.Usuarios", "InvitacionPendientes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Usuarios", "InvitacionPendientes", c => c.Int(nullable: false));
            AddColumn("dbo.Usuarios", "InvitacionActivas", c => c.Int(nullable: false));
            DropForeignKey("dbo.Invitaciones", "Usuario_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Invitaciones", "Invitado_Id", "dbo.Usuarios");
            DropIndex("dbo.Invitaciones", new[] { "Usuario_Id" });
            DropIndex("dbo.Invitaciones", new[] { "Invitado_Id" });
            DropColumn("dbo.Usuarios", "Invitaciones");
            DropTable("dbo.Invitaciones");
        }
    }
}
