namespace VeinsBuster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateInvitationsAndUser : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Invitaciones", newName: "Invitacions");
            AddColumn("dbo.Invitacions", "Estatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.Usuarios", "InvitacionesPendientes", c => c.Int(nullable: false));
            AlterColumn("dbo.Usuarios", "Puntos", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Usuarios", "Puntos", c => c.Int(nullable: false));
            DropColumn("dbo.Usuarios", "InvitacionesPendientes");
            DropColumn("dbo.Invitacions", "Estatus");
            RenameTable(name: "dbo.Invitacions", newName: "Invitaciones");
        }
    }
}
