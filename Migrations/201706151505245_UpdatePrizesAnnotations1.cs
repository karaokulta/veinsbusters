namespace VeinsBuster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePrizesAnnotations1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transaccions", "FechaUso", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transaccions", "FechaUso", c => c.DateTime(nullable: false));
        }
    }
}
