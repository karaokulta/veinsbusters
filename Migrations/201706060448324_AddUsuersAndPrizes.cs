namespace VeinsBuster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsuersAndPrizes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Premios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 64),
                        Descripcion = c.String(maxLength: 128),
                        Costo = c.Int(nullable: false),
                        TipoId = c.Byte(nullable: false),
                        Estatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoPremios", t => t.TipoId, cascadeDelete: true)
                .Index(t => t.TipoId);
            
            CreateTable(
                "dbo.TipoPremios",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 64),
                        Email = c.String(maxLength: 128),
                        Password = c.String(maxLength: 64),
                        Puntos = c.Int(nullable: false),
                        InvitacionActivas = c.Int(nullable: false),
                        InvitacionPendientes = c.Int(nullable: false),
                        Codigo = c.String(),
                        Estatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Premios", "TipoId", "dbo.TipoPremios");
            DropIndex("dbo.Premios", new[] { "TipoId" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.TipoPremios");
            DropTable("dbo.Premios");
        }
    }
}
